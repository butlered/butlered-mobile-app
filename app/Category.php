<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'category';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'image'
    ];




    /**
     * Get the product record associated with the user.
     */
    public function products()
    {
        return $this->hasMany('App\Product', 'fk_category_id', 'id');
    }
}
