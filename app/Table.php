<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Table extends Model
{
    /**
    * The table associated with the model.
    *
    * @var string
    */

    protected $table = 'table';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fk_restaurant_id', 'number', 'seats', 'QRpassword'
    ];


    /**
     * Get the restaurant record associated with the user.
     */
    public function restaurant()
    {
        return $this->hasOne('App\Restaurant', 'id', 'fk_restaurant_id');
    }
}
