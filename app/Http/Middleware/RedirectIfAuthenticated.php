<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Table;
use App\User;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $user = new \App\User();

        if (Auth::guard($guard)->check()) {
            if($user->fk_table_id == null){
                return redirect('/tableLogin');
            }else{
                return redirect('/');
            }
        }
        return $next($request);
    }
}
