<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Category;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use \App\Bill;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = \App\Category::all();
        return view('home', array("categories" => $categories));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $orders_raw = Input::get('data');
        $orders = array();
        $table_id = Auth::user()['fk_table_id'];
        $bill_id = \App\Bill::where(['fk_table_id' => $table_id, 'status' => 'open', 'paid' => 0 ])->first()->id;


        foreach ($orders_raw as $key => $order) {
            $orders[$key] = ['fk_bill_id' => $bill_id, 'fk_product_id' => $order[0], 'amount' => $order[1], 'processed' => 0, 'created_at' => date('Y-m-d H:i:s')];
        }

        print_r($orders);
        Bill::find($bill_id)->products()->attach($orders); // Eloquent approach
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $table_id = Auth::user()['fk_table_id'];
        $bill_id = Bill::where(['fk_table_id' => $table_id, 'status' => 'open', 'paid' => 0 ])->first()->id;
        $products = Bill::find($bill_id)->first()->products()->get();

        return view('bill', array("products" => $products));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
