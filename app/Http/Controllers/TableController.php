<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Table;
use App\User;
use App\Bill;
use Auth;
use Sentinel;

class TableController extends Controller
{
    public function index(Request $request) {

		return view('tableScan');
	}

	public function checkTable(Request $request) {
		 $result =0;
			if ($request->data) {
				$table = Table::where('QRpassword',$request->data)->first();

				if ($table) {
                    $user = Auth::user();
                    $user->fk_table_id = $table['id'];
                    $user->save();

                    $bill_id;
                    // Get unpayed bill

                    if ($bill = Bill::where(['fk_table_id' => $table['id'], 'paid' => 0])->first()) {
                        $bill_id = $bill->id;
                    } else {
                        // Create new bill
                        $new_bill = new Bill;
                        $new_bill->fk_table_id = $table['id'];
                        $new_bill->status = 'open';
                        $new_bill->paid = 0;
                        $new_bill->save();
                        $bill_id = $new_bill->id;
                    }

                    // print_r($bill_id);

                    if(!User::find($user['id'])->bills()->where(['fk_user_id' => $user['id'], 'fk_bill_id' => $bill_id])->exists()) {
                        User::find($user['id'])->bills()->attach($bill_id);
                    }

				    $result = 1;
				 }else{
				 	$result = 0;
				 }


			}
			return $result;
	}
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
