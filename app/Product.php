<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'product';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fk_category_id', 'name', 'price', 'image', 'fk_restaurant_id'
    ];



    /**
     * Get the category record associated with the user.
     */
    public function category()
    {
        return $this->hasOne('App\Category', 'id', 'fk_category_id');
    }

    /**
     * Get the restaurant record associated with the user.
     */
    public function restaurant()
    {
        return $this->hasOne('App\Restaurant', 'id', 'fk_restaurant_id');
    }

    /**
     * The bills that belong to the user.
     */
    public function bills()
    {
        return $this->belongsToMany('App\Bill', 'bills_products', 'fk_product_id', 'fk_bill_id');
    }
}
