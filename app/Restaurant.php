<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'restaurant';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'city', 'table_amount', 'phone', 'mail', 'address'
    ];


    /**
     * Get the tables for the blog post.
     */
    public function tables()
    {
        return $this->hasMany('App\Table', 'fk_restaurant_id', 'id');
    }
}
