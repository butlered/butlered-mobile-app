<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'bill';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fk_table_id', 'status', 'paid'
    ];



    /**
     * Get the table record associated with the user.
     */
    public function table()
    {
        return $this->hasOne('App\Table', 'id', 'fk_table_id');
    }

    /**
     * The users that belong to the user.
     */
    public function users()
    {
        return $this->belongsToMany('App\User', 'users_bills', 'fk_bill_id', 'fk_user_id');
    }

    /**
     * The products that belong to the user.
     */
    public function products()
    {
        return $this->belongsToMany('App\Product', 'bills_products', 'fk_bill_id', 'fk_product_id')->withPivot('amount');
    }
}
