<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'provider', 'provider_id', 'QRpassword'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the table record associated with the user.
     */
    public function table()
    {
        return $this->hasOne('App\Table', 'id', 'fk_table_id');
    }

    /**
     * The bills that belong to the user.
     */
    public function bills()
    {
        return $this->belongsToMany('App\Bill', 'users_bills', 'fk_user_id', 'fk_bill_id');
    }

}
