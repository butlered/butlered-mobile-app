$(document).ready(function(){

	// Initialize collapse button
	  $('.button-collapse').sideNav({
		  menuWidth: 250, // Default is 300
		  edge: 'left', // Choose the horizontal origin
		  closeOnClick: true, // Closes side-nav on <a> clicks, useful for Angular/Meteor
		  draggable: true, // Choose whether you can drag to open on touch screens,
		  onOpen: function(el) { /* Do Stuff*/ }, // A function to be called when sideNav is opened
		  onClose: function(el) { /* Do Stuff*/ }, // A function to be called when sideNav is closed
		}
	  );
	  // Initialize collapsible (uncomment the line below if you use the dropdown variation)
	  //$('.collapsible').collapsible();

	$('.product').on('click touchstart', function() {
		$(this).parent('.product-container').find('.amount_selector').slideToggle();
	})

	$('.amount_selector .min').on('click touchstart', function() {
		amount = $(this).parent(".amount_selector").find("input").val();
		if (amount > 0) {
			amount--;
			$(this).parents(".amount_selector").find("input").val(amount);
			$(this).parents(".product-container").data("amount", amount);
		}
		if(amount < 1){
			$(this).parents(".product-container").removeClass('selected');
		}
	});

	$('.amount_selector .plus').on('click touchstart', function() {
		amount = $(this).parent(".amount_selector").find("input").val();
		amount++;
		$(this).parents(".amount_selector").find("input").val(amount);
		$(this).parents(".product-container").data('amount', amount);
		$(this).parents(".product-container").addClass('selected');
	});


	$('button').on('click', function() {
		var data = [];
		$(".product-container.selected").each(function( index ) {
			var product_id = $(this).data('product-id');
			var amount = $(this).data('amount');
			var order = [product_id, amount];
			data[index] = order;
		});

		if (data.length > 0) {
			$.ajax({
				type: "POST",
				// cache: false,
				url : "/order",
				data: {data:data,"_token": $('meta[name=csrf-token]').attr('content')},

				success: function(data) {
					Materialize.toast('Order placed', 4000);

					$(".product-container.selected").each(function( index ) {
						$(this).data('product-id', 0);
						$(this).data('amount', 0);
						$(this).removeClass('selected');
						$(this).find('.amount_selector').slideUp();
						$(this).find(".amount_selector").find("input").val(0);
					});
				}
			})
		} else {
			Materialize.toast('Please select something to order', 4000);
		}
	})
});
