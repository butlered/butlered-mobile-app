@extends('layouts/guest')

@section('content')
    <!-- Header -->
    <div class="container-fluid header_se">
        <div class="col-md-8">
            <div class="row">
                <div id="reader" class="center-block" style="width:100vw;height:100vh">
                </div>
            </div>
            <div class="row">
                <div id="message" class="text-center"></div>
            </div>
        </div>
    </div>
    <!-- /.Header -->
@endsection


@section('scripts')
<script type="text/javascript" src="{{ URL::asset('js/qr/jsqrcode-combined.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/qr/html5-qrcode.min.js') }}"></script>

<script type="text/javascript">
   $('#reader').html5_qrcode(function(data){
        $('#message').html('<span class="text-success send-true">Scanning now....</span>');
        if (data!='') {
            $.ajax({
                type: "POST",
                cache: false,
                url : "{{action('TableController@checkTable')}}",
                data: {data:data},
                data: {"_token": "{{ csrf_token() }}",data:data},

                success: function(data) {
                    if (data==1) {
                        //location.reload()
                        $(location).attr('href', '{{url('/')}}');
                    }else{
                        return confirm('Deze QR Code is ongeldig');
                    }
                }
            })
        }else{return confirm('There is no  data');}
    },
    function(error){
       $('#message').html('Scaning now ....'  );
    }, function(videoError){
       $('#message').html('<span class="text-danger camera_problem"> there was a problem with your camera </span>');
    }
);
</script>

@endsection
