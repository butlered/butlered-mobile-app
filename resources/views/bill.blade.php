@extends('layouts.app')

@section('side_content')
    <div class="main-nav-menu">
        <a class="mnavt_active" href="!#">Bill</a>
        <a class="mnavt" href="/">Order</a>
    </div>
@endsection
@section('content')
    <div class="bill-container">
        @foreach ($products as $product)
            <div class="bill-field">
            	<div class="Boxes z-depth-5 orderbox">
            		<div class="product-container">
                    	<div class="product"> <img src="{{url('/')}}/images/products/{{$product->image}}">
                      		<p>{{$product->name}}</p>
            				<span>X{{$product->pivot->amount}}</span>
                      		<p>&euro; {{$product->price}}</p>
                    	</div>
            		</div>
            	</div>
            </div>
        @endforeach
    </div>

@endsection
