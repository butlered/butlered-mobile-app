@extends('layouts.guest')

@section('content')
    <div class="main-nav-login-signup">
        <a class="mnavt_active" href="{{ url('/register') }}">Sign up</a>
        <a class="mnavt" href="{{ url('/login') }}">Login</a>
    </div>

    <div class="LogoTop">
        <img src="Images/Artboard 1.svg" alt="Jameslogo" style="width:200px;">
    </div>

    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
    {{ csrf_field() }}

    <div id="FullnameBox" class="Boxes z-depth-5">
        <input type="text" name="name" placeholder="Full name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Full name'" required>
    </div>
    <div id="EmailBox2" class="Boxes z-depth-5">
        <input type="email" name="email" placeholder="E-mail" onfocus="this.placeholder = ''" onblur="this.placeholder = 'E-mail'" required>
    </div>
    <div id="PasswordBox2" class="Boxes z-depth-5">
        <input type="password" name="password" placeholder="Password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Password'" required>
    </div>

        <button id="signup" type="submit" class="Boxes z-depth-5">Signup</button>

    </form>

    <a href="{{ url('/auth/facebook') }}">
        <div id="FacebookBox2" class="Boxes z-depth-5">
            <span>LOGIN WITH FACEBOOK</span>
        </div>
    </a>
    <a href="{{ url('/auth/google') }}">
        <div id="GoogleBox2" class="Boxes z-depth-5">
            <span>LOGIN WITH GOOGLE+</span>
        </div>
    </a>

@endsection
