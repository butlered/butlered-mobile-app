@extends('layouts.guest')

@section('content')

    <div class="main-nav-login-signup">
        <a class="mnavt" href="{{ url('/register') }}">Sign up</a>
        <a class="mnavt_active" href="{{ url('/login') }}">Login</a>
    </div>

    <div class="LogoTop">
        <img src="Images/Artboard 1.svg" alt="Jameslogo" style="width:200px;">
    </div>

    <form method="POST" action="{{ route('login') }}">
        {{ csrf_field() }}

        <div id="EmailBox" class="Boxes z-depth-5">
            <input type="email" name="email" placeholder="E-mail" onfocus="this.placeholder = ''" onblur="this.placeholder = 'E-mail'" required>
            @if ($errors->has('email'))
                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
            @endif
        </div>

        <div id="PasswordBox" class="Boxes z-depth-5">
            <input type="password" name="password" placeholder="Password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Password'" required>
            @if ($errors->has('password'))
                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
            @endif
        </div>
        <button id="Loginbox" type="submit" class="Boxes z-depth-5">Login</button>
    </form>


    <a href="{{ url('/auth/facebook') }}">
        <div id="FacebookBox2" class="Boxes z-depth-5">
            <span>LOGIN WITH FACEBOOK</span>
        </div>
    </a>
    <a href="{{ url('/auth/google') }}">
        <div id="GoogleBox2" class="Boxes z-depth-5">
            <span>LOGIN WITH GOOGLE+</span>
        </div>
    </a>


    {{--!--Import jQuery before materialize.js-->--}}
    <script type="text/javascript" src="//code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="{{ asset('css/materialize.min.css') }}zjs/materialize.min.js"></script>
    <script>
        if ('serviceWorker' in navigator) {
            console.log("Will the service worker register?");
            navigator.serviceWorker.register('sw.js')
                .then(function(reg) {
                    console.log("Yes, it did.");
                }).catch(function(err) {
                console.log("No it didn't. This happened: ", err)
            });
        }
    </script>

@endsection
