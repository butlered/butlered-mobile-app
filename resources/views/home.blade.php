@extends('layouts.app')

@section('side_content')
    <div class="main-nav-menu">
        <a class="mnavt" href="/bill">Bill</a>
        <a class="mnavt_active" href="!#">Order</a>
    </div>
@endsection
@section('content')

<div class="rij">
	<div class="menu-container">
	   <ul id="tabs-swipe-demo" class="tabs tabs-fixed-width">
           @foreach ($categories as $category)
               <li class="tab col s3"><a href="#swipe-{{$category['id']}}">{{$category['name']}}</a></li>
           @endforeach
	  </ul>
        @foreach ($categories as $category)
            <div id="swipe-{{$category['id']}}" class="menu-container">
                @foreach ($category->products as $product)
                    <div class="product-container" data-product-id="{{$product['id']}}">
                        <div class="product">
                            <img src="{{url('/')}}/images/products/{{$product->image}}">
                            <p>{{$product['name']}}</p>
                            <p>€ {{$product['price']}}</p>
                        </div>
                        <div class="amount_selector">
                            <i class="material-icons min">remove</i>
                            <input type="number" min="0" value="0">
                            <i class="material-icons plus">add</i>
                        </div>
                    </div>
                @endforeach
            </div>
        @endforeach
	</div>
</div>

<div class="order">
    <button type="button">Order</button>
</div>
@endsection
