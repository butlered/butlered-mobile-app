<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <title>James</title>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    {{--font--}}
    <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
    <!--Import materialize.css-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css" rel="stylesheet">

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="manifest" href="{{ asset('js/manifest.json') }}" />
    <link rel="stylesheet" href="{{ asset('css/Style.css') }}" />
    <link rel="sidebar" href="{{ asset('js/sidenav.js') }}" />
    <meta name="theme-color" content="#383D3B" />
    <meta name="mobile-web-app-capable" content="yes">
</head>
<body>
{{--Load content--}}
@yield('content')

<script src="{{ asset('js/app.js') }}"></script>

{{--load scripts--}}
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
@yield('scripts')

</body>
</html>
