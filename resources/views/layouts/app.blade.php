<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'James') }}</title>

    <title>James</title>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    {{--font--}}
    <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
    <!--Import materialize.css-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css" rel="stylesheet">

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="manifest" href="{{ asset('js/manifest.json') }}" />
    <link rel="stylesheet" href="{{ asset('css/app.css') }}" />
    <meta name="theme-color" content="#383D3B" />
    <meta name="mobile-web-app-capable" content="yes">
</head>
<body>
<a href="#" data-activates="slide-out" class="button-collapse"><i class="material-icons">menu</i></a>
<ul id="slide-out" class="side-nav">
    <li class="nav-main-head"><div class="user-view">
      <div class="background">
        <img src="Images/De-Vooruitgang.jpg">
      </div>
      <!--<a href="#!ftuser"><img class="circle" src="images/Artboard 1.svg"></a>-->
      <a href="#!name"><span class="white-text name">{{ Auth::user()->name}}</span></a>
      <a href="#!email"><span class="white-text email">{{ isset(Auth::user()->name) ? Auth::user()->email : Auth::user()->email }}</span></a>
    </div></li>
    {{-- <li><a class="waves-effect" href="#!">Account</a></li> --}}
    <li><a class="waves-effect" href="/">Bestellen</a></li>
    <li><a class="waves-effect" href="/bills">Rekeningen</a></li>
	{{-- <li><a class="waves-effect" href="#!">Sluit tafel</a></li> --}}
	<li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a></li>

    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>

</ul>
{{--Load content--}}
@yield('side_content')
<div class="panel-body">
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
</div>
@yield('content')

<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/materialize.min.js') }}"></script>
<link rel="sidebar" href="{{ asset('js/sidenav.js') }}" />

{{--load scripts--}}
@yield('scripts')

</body>
</html>
