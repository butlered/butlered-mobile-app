/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(1);
module.exports = __webpack_require__(2);


/***/ }),
/* 1 */
/***/ (function(module, exports) {

$(document).ready(function () {

	// Initialize collapse button
	$('.button-collapse').sideNav({
		menuWidth: 250, // Default is 300
		edge: 'left', // Choose the horizontal origin
		closeOnClick: true, // Closes side-nav on <a> clicks, useful for Angular/Meteor
		draggable: true, // Choose whether you can drag to open on touch screens,
		onOpen: function onOpen(el) {/* Do Stuff*/}, // A function to be called when sideNav is opened
		onClose: function onClose(el) {/* Do Stuff*/} // A function to be called when sideNav is closed
	});
	// Initialize collapsible (uncomment the line below if you use the dropdown variation)
	//$('.collapsible').collapsible();

	$('.product').on('click touchstart', function () {
		$(this).parent('.product-container').find('.amount_selector').slideToggle();
	});

	$('.amount_selector .min').on('click touchstart', function () {
		amount = $(this).parent(".amount_selector").find("input").val();
		if (amount > 0) {
			amount--;
			$(this).parents(".amount_selector").find("input").val(amount);
			$(this).parents(".product-container").data("amount", amount);
		}
		if (amount < 1) {
			$(this).parents(".product-container").removeClass('selected');
		}
	});

	$('.amount_selector .plus').on('click touchstart', function () {
		amount = $(this).parent(".amount_selector").find("input").val();
		amount++;
		$(this).parents(".amount_selector").find("input").val(amount);
		$(this).parents(".product-container").data('amount', amount);
		$(this).parents(".product-container").addClass('selected');
	});

	$('button').on('click', function () {
		var data = [];
		$(".product-container.selected").each(function (index) {
			var product_id = $(this).data('product-id');
			var amount = $(this).data('amount');
			var order = [product_id, amount];
			data[index] = order;
		});

		if (data.length > 0) {
			$.ajax({
				type: "POST",
				// cache: false,
				url: "/order",
				data: { data: data, "_token": $('meta[name=csrf-token]').attr('content') },

				success: function success(data) {
					Materialize.toast('Order placed', 4000);

					$(".product-container.selected").each(function (index) {
						$(this).data('product-id', 0);
						$(this).data('amount', 0);
						$(this).removeClass('selected');
						$(this).find('.amount_selector').slideUp();
						$(this).find(".amount_selector").find("input").val(0);
					});
				}
			});
		} else {
			Materialize.toast('Please select something to order', 4000);
		}
	});
});

/***/ }),
/* 2 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ })
/******/ ]);