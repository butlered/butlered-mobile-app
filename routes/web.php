<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

// OAuth Routes
Route::get('auth/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\LoginController@handleProviderCallback');


Route::group(['middleware' => 'auth' ], function () {
    Route::get('/', 'OrderController@index')->name('home');

    // Table Routes
    Route::get('tableLogin', ['uses' => 'TableController@index'])->name('TableLogin');
    Route::post('tableLogin', ['uses' => 'TableController@checkTable']);

    // Order
    Route::post('/order','OrderController@create');
    Route::get('/bill','OrderController@show');

});
