<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('restaurant')->insert([
            'id' => 1,
            'name' => 'Brownies & Bart',
            'city' => 'Eindhoven',
            'table_amount' => 12,
            'phone' => '0612345678',
            'mail' => 'info@james.com',
            'address' => 'Bartelomeuslaan 12',
        ]);

        DB::table('table')->insert([
            'id' => 1,
            'fk_restaurant_id' => 1,
            'number' => 1,
            'seats' => '4',
            'QRpassword' => 'kasdDdDA@h25%asdf$hAR5vv$hdfjf@h4$',
        ]);
    }
}
