<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Init extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('users');


        DB::unprepared("
            -- MySQL Script generated by MySQL Workbench
            -- 12/12/17 09:59:56
            -- Model: New Model    Version: 1.0
            -- MySQL Workbench Forward Engineering

            SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
            SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
            SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

            -- -----------------------------------------------------
            -- Schema james
            -- -----------------------------------------------------

            -- -----------------------------------------------------
            -- Schema james
            -- -----------------------------------------------------
            CREATE SCHEMA IF NOT EXISTS `james` DEFAULT CHARACTER SET utf8 ;
            USE `james` ;

            -- -----------------------------------------------------
            -- Table `restaurant`
            -- -----------------------------------------------------
            CREATE TABLE IF NOT EXISTS `restaurant` (
              `id` INT NOT NULL AUTO_INCREMENT,
              `name` VARCHAR(45) NOT NULL,
              `city` VARCHAR(45) NOT NULL,
              `table_amount` INT NOT NULL,
              `phone` VARCHAR(15) NOT NULL,
              `mail` VARCHAR(255) NOT NULL,
              `address` VARCHAR(255) NULL,
              PRIMARY KEY (`id`))
            ENGINE = InnoDB;


            -- -----------------------------------------------------
            -- Table `table`
            -- -----------------------------------------------------
            CREATE TABLE IF NOT EXISTS `table` (
              `id` INT NOT NULL AUTO_INCREMENT,
              `fk_restaurant_id` INT NOT NULL,
              `number` INT NOT NULL,
              `seats` VARCHAR(45) NOT NULL,
              `QRpassword` VARCHAR(255) NOT NULL,
              PRIMARY KEY (`id`, `fk_restaurant_id`),
              INDEX `fk_table_restaurant1_idx` (`fk_restaurant_id` ASC),
              UNIQUE INDEX `QRpassword_UNIQUE` (`QRpassword` ASC),
              CONSTRAINT `fk_table_restaurant1`
                FOREIGN KEY (`fk_restaurant_id`)
                REFERENCES `restaurant` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
            ENGINE = InnoDB;


            -- -----------------------------------------------------
            -- Table `users`
            -- -----------------------------------------------------
            CREATE TABLE IF NOT EXISTS `users` (
              `id` INT NOT NULL AUTO_INCREMENT,
              `name` VARCHAR(255) NOT NULL,
              `email` VARCHAR(255) NOT NULL,
              `password` VARCHAR(255) NULL,
              `provider` VARCHAR(255) NULL,
              `provider_id` VARCHAR(255) NULL,
              `fk_table_id` INT NULL,
              `remember_token` VARCHAR(100) NULL,
              PRIMARY KEY (`id`),
              INDEX `fk_user_table1_idx` (`fk_table_id` ASC),
              CONSTRAINT `fk_user_table1`
                FOREIGN KEY (`fk_table_id`)
                REFERENCES `table` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
            ENGINE = InnoDB;


            -- -----------------------------------------------------
            -- Table `bill`
            -- -----------------------------------------------------
            CREATE TABLE IF NOT EXISTS `bill` (
              `id` INT NOT NULL AUTO_INCREMENT,
              `fk_table_id` INT NOT NULL,
              `status` ENUM('open', 'closed') NOT NULL,
              `paid` TINYINT(1) NOT NULL,
              PRIMARY KEY (`id`, `fk_table_id`),
              INDEX `fk_bill_table1_idx` (`fk_table_id` ASC),
              CONSTRAINT `fk_bill_table1`
                FOREIGN KEY (`fk_table_id`)
                REFERENCES `table` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
            ENGINE = InnoDB;


            -- -----------------------------------------------------
            -- Table `category`
            -- -----------------------------------------------------
            CREATE TABLE IF NOT EXISTS `category` (
              `id` INT NOT NULL AUTO_INCREMENT,
              `name` VARCHAR(45) NOT NULL,
              `image` VARCHAR(255) NULL,
              PRIMARY KEY (`id`))
            ENGINE = InnoDB;


            -- -----------------------------------------------------
            -- Table `product`
            -- -----------------------------------------------------
            CREATE TABLE IF NOT EXISTS `product` (
              `id` INT NOT NULL AUTO_INCREMENT,
              `fk_category_id` INT NOT NULL,
              `name` VARCHAR(45) NOT NULL,
              `price` DECIMAL(8,2) NOT NULL,
              `image` VARCHAR(255) NOT NULL,
              `fk_restaurant_id` INT NOT NULL,
              PRIMARY KEY (`id`, `fk_category_id`),
              INDEX `fk_product_category1_idx` (`fk_category_id` ASC),
              INDEX `fk_product_restaurant1_idx` (`fk_restaurant_id` ASC),
              CONSTRAINT `fk_product_category1`
                FOREIGN KEY (`fk_category_id`)
                REFERENCES `category` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION,
              CONSTRAINT `fk_product_restaurant1`
                FOREIGN KEY (`fk_restaurant_id`)
                REFERENCES `restaurant` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
            ENGINE = InnoDB;


            -- -----------------------------------------------------
            -- Table `bills_products`
            -- -----------------------------------------------------
            CREATE TABLE IF NOT EXISTS `bills_products` (
              `id` INT NOT NULL AUTO_INCREMENT,
              `fk_bill_id` INT NOT NULL,
              `fk_product_id` INT NOT NULL,
              `created_at` VARCHAR(45) NOT NULL,
              `processed` TINYINT(1) NOT NULL,
              PRIMARY KEY (`id`, `fk_bill_id`, `fk_product_id`),
              INDEX `fk_bills_products_bill1_idx` (`fk_bill_id` ASC),
              INDEX `fk_bills_products_product1_idx` (`fk_product_id` ASC),
              CONSTRAINT `fk_bills_products_bill1`
                FOREIGN KEY (`fk_bill_id`)
                REFERENCES `bill` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION,
              CONSTRAINT `fk_bills_products_product1`
                FOREIGN KEY (`fk_product_id`)
                REFERENCES `product` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
            ENGINE = InnoDB;


            -- -----------------------------------------------------
            -- Table `users_bills`
            -- -----------------------------------------------------
            CREATE TABLE IF NOT EXISTS `users_bills` (
              `fk_user_id` INT NOT NULL,
              `fk_bill_id` INT NOT NULL,
              PRIMARY KEY (`fk_user_id`, `fk_bill_id`),
              INDEX `fk_user_has_bill_bill1_idx` (`fk_bill_id` ASC),
              INDEX `fk_user_has_bill_user_idx` (`fk_user_id` ASC),
              CONSTRAINT `fk_user_has_bill_user`
                FOREIGN KEY (`fk_user_id`)
                REFERENCES `users` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION,
              CONSTRAINT `fk_user_has_bill_bill1`
                FOREIGN KEY (`fk_bill_id`)
                REFERENCES `bill` (`id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
            ENGINE = InnoDB;


            SET SQL_MODE=@OLD_SQL_MODE;
            SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
            SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bills_products');
        Schema::dropIfExists('users_bills');
        Schema::dropIfExists('bill');
        Schema::dropIfExists('product');
        Schema::dropIfExists('category');
        Schema::drop('user');
        Schema::dropIfExists('table');
        Schema::drop('restaurant');

    }
}
