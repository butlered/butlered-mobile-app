<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdToUsersBills extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('users_bills');

        Schema::create('users_bills', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('fk_user_id');
            $table->integer('fk_bill_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_bills');

        DB::unprepared("
         CREATE TABLE `users_bills` (
           `fk_user_id` int(11) NOT NULL,
           `fk_bill_id` int(11) NOT NULL,
           `created_at` datetime DEFAULT NULL,
           `updated_at` datetime DEFAULT NULL,
           `id` int(11) DEFAULT NULL,
           PRIMARY KEY (`fk_user_id`,`fk_bill_id`),
           KEY `fk_user_has_bill_bill1_idx` (`fk_bill_id`),
           KEY `fk_user_has_bill_user_idx` (`fk_user_id`),
           CONSTRAINT `fk_user_has_bill_bill1` FOREIGN KEY (`fk_bill_id`) REFERENCES `bill` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
           CONSTRAINT `fk_user_has_bill_user` FOREIGN KEY (`fk_user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
         ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
         ");

    }
}
